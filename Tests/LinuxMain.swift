import XCTest

import demo_spmTests

var tests = [XCTestCaseEntry]()
tests += demo_spmTests.allTests()
XCTMain(tests)

import XCTest
@testable import demo_spm

final class demo_spmTests: XCTestCase {
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(demo_spm().text, "Hello, World!")
    }

    static var allTests = [
        ("testExample", testExample),
    ]
}
